function [ m ] = smalltipangle(dB0,B1,m)

    m0    = 1;
    dt    = 10^-7;       %0.1 micro second
    gamma = 2*pi*42.577*10^6; % rad*Hz/Tesla
    
    m = 1i*dt*gamma*((B1*m0)-(dB0*m))+m;

end
